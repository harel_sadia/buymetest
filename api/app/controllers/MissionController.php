<?php
use Models\Mission;

class MissionController extends \BaseController
{
    /**
     * Mission instance
     *
     * @var Mission
     */
    protected $mission;
    /**
     * Create new instance of Mission using inflection
     *
     * @param Mission $mission
     */
    public function __construct(Mission $mission)
    {
        $this->mission = $mission;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $missions = $this->mission->all()->toArray();
        return Response::json(compact('missions'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data = Input::get('mission');
        $mission = $this->mission->create($data);
        $mission = $mission->toArray();
        return Response::json(compact('mission'), 201);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data = Input::get('mission');
        $mission = $this->mission->findOrFail($id);
        $mission->update($data);
        $mission = $mission->toArray();
        return Response::json(compact('mission'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->mission->destroy($id);
        return Response::make(null, 204);
    }
}
