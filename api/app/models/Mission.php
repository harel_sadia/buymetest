<?php

namespace Models;

use Eloquent;

class Mission extends Eloquent
{
    public $timestamps = false;

    // protected $table = 'missions';
    protected $fillable = array('text', 'isCompleted');
}
