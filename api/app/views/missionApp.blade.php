<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Missions list</title>
    <script src="{{public_path('js/lib/jquery-1.11.2.js')}}"></script>
    <script src="{{public_path('js/lib/handlebars-v1.3.0.js')}}"></script>
    <script src="{{public_path('js/lib/ember.js')}}"></script>
    <script src="{{public_path('js/lib/ember-data.js')}}"></script>

    <link rel="stylesheet" href="{{public_path('css/style.css')}}">

</head>

<body>

  <script type="text/x-handlebars" data-template-name="missions">
    <section id="missionapp">
        <header id="header">
            <h1>missions</h1>
            @{{input type="text" id="new-mission" placeholder="Add new mission?" value=title action="createMission"}}
        </header>

        <section id="main">
            <ul id="mission-list">
                <ul id="mission-list">
                    @{{#each mission in model itemController="mission"}}
                    <li @{{bind-attr class="mission.isCompleted:completed mission.isEditing:editing" }}>

                        @{{#if mission.isEditing}}

                        @{{edit-mission value=mission.title focus-out="acceptChanges" insert-newline="acceptChanges"  class="edit"}}

                        @{{else}} 
                        @{{input type="checkbox" checked=mission.isCompleted class="toggle"}}

                        <label @{{action "editMission" on="doubleClick" }}>@{{mission.title}}</label>
                        <button @{{action "removeMission"}} class="destroy"></button>
                        
                        @{{/if}}
                    </li>
                    @{{/each}}
                </ul>
            </ul>

            <input type="checkbox" id="toggle-all">
        </section>

        <footer id="footer">
            <span id="mission-count">
                <strong>@{{remaining}}</strong> @{{inflection}} left
            </span>

        </footer>


    </section>

    </script>
   
    

    <script src="{{public_path('js/application.js')}}"></script>
    <script src="{{public_path('js/router.js')}}"></script>
    <!-- models -->
    <script src="{{public_path('js/models/mission.js')}}"></script>
    <script src="{{public_path('js/controllers/MissionsController.js')}}"></script>
    <script src="{{public_path('js/controllers/MissionController.js')}}"></script>

    <!-- helpers -->
    <script src="{{public_path('js/helpers/MissionEditHelper.js')}}"></script>

</body>

</html>