Missions.Mission = DS.Model.extend({
  text: DS.attr('string'),
  isCompleted: DS.attr('boolean')
});
