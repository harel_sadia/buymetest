Missions.EditMissionView = Ember.TextField.extend({
    didInsertElement: function() {
      this.$().focus();
    }
  });
  
  Ember.Handlebars.helper('edit-mission', Missions.EditMissionView);