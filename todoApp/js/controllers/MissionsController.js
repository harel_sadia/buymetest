Missions.MissionsController = Ember.ArrayController.extend({
    actions: {
        createMission: function () {
            // Get the new Mission text
            var text = this.get('text');
            if (!text.trim()) { return; }

            // Create the new Mission model
            var mission = this.store.createRecord('mission', {
                text: text,
                isCompleted: false
            });

            // Clear the "text" text field
            this.set('text', '');

            // Save the new model
            mission.save();
        }
    },
    remaining: function () {
        return this.filterBy('isCompleted', false).get('length');
    }.property('@each.isCompleted'),
    done: function () {
        return this.filterBy('isCompleted', true).get('length');
    }.property('@each.isCompleted'),
});