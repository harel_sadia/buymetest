Missions.MissionController = Ember.ObjectController.extend({
   
    actions: {
        editMission: function () {
            this.set('isEditing', true);
        },
        removeMission: function () {
            var mission = this.get('model');
            mission.deleteRecord();
            mission.save();
        },
        acceptChanges: function () {
            this.set('isEditing', false);

            if (Ember.isEmpty(this.get('model.text'))) { //if text is empty
                this.send('removeMission'); 
            } else {
                this.get('model').save();
            }
        },
    },
    isCompleted: function (key, value) {
        var model = this.get('model');

        if (value === undefined) {
            // property being used as a getter
            return model.get('isCompleted');
        } else {
            // property being used as a setter
            model.set('isCompleted', value);
            model.save();
            return value;
        }
    }.property('model.isCompleted'),
    isEditing: false,
});
