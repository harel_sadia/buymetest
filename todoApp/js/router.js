Missions.Router.map(function () {
  this.resource('Missions', { path: '/' });
});

Missions.MissionsRoute = Ember.Route.extend({
  model: function () {
    return this.store.find('mission'); 
  }
});
